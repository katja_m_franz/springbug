package de.example.springBug.service;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@RestController
@RequestMapping("/service/springBug/")
public class SpringBugRestController {

  @PostMapping(value = "uploadFiles", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = "application/json")
  public String testFiles(HttpServletRequest request) {

    int files = 0;
    if (ServletFileUpload.isMultipartContent(request)) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      files = multipartRequest.getFileMap().size();
    }
    if (files > 0) {
      return "OK";
    } else {
      return "NOT OK NO FILES";
    }
  }
}