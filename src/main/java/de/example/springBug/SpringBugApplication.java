package de.example.springBug;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import de.example.springBug.service.ServiceConfig;

@SpringBootApplication
@Configuration
@Import({ ServiceConfig.class })
public class SpringBugApplication {

  public static void main(String[] args) {

    SpringApplication.run(SpringBugApplication.class, args);
  }

}